# frozen_string_literal: true

module ReviewTanuki
  class Synchronizer
    CANONICAL_GITLAB_PROJECT = 'gitlab-org/gitlab'
    DEFAULT_OPTS = {
      project: 'timofurrer/growth-and-development',
      client: Gitlab.client,
      dry_run: false
    }.freeze

    attr_reader :issue

    def initialize(issue_iid, opts: {})
      @opts = DEFAULT_OPTS.merge(opts)

      @issue = TraineeIssue.new(client, project, issue_iid)
    end

    def execute
      untracked_merge_requests.each do |mr|
        track(mr)
      end
    end

    private

    def untracked_merge_requests
      issue_notes.each_with_object(mrs_in_review) do |note, merge_requests|
        return [] if merge_requests.empty?

        next if note.system
        next if note.author.id != issue.trainee.id

        merge_requests.delete_if { |mr| review_note_for_mr?(note, mr) }
      end
    end

    def mrs_in_review
      client.user_merge_requests(
        state: :opened,
        scope: :all,
        project: CANONICAL_GITLAB_PROJECT,
        my_reaction_emoji: emoji
      ).map { |mr| MergeRequest.new(client, CANONICAL_GITLAB_PROJECT, mr.iid) }
    end

    def review_note_for_mr?(note, merge_request)
      note_body = note.body.chomp

      preamble = note_body.split("\n", 2).map(&:chomp)
      preamble.any? { |line| line.include?("<!-- #{merge_request.short_link} -->") }
    end

    def track(merge_request)
      logger.info("Creating new note for #{merge_request.short_link}")

      issue.create_note(merge_request.review_body) unless dry_run?
    end

    def issue_notes
      issue.notes.auto_paginate
    end

    def dry_run?
      @opts[:dry_run]
    end

    def client
      @opts[:client]
    end

    def project
      @opts[:project]
    end

    def emoji
      @opts[:emoji]
    end

    def logger
      @logger ||= Logger.new($stderr)
    end
  end
end
